package data;

import java.util.ArrayList;

public class PersonList {
    ArrayList<Person> personList = new ArrayList<>();

    public PersonList() {
    }

    public void addPerson(Person person){
        personList.add(person);
    }

    public int getNumPersons(){
        return personList.size();
    }


}
