package data;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.layout.*;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ScheduleController implements Initializable {

    @FXML
    private TabPane TabPane;

    @FXML
    private GridPane calenderGridPane;

    @FXML
    private ListView SignupList;

    @FXML
    private TextArea PersonInfoTextArea;

    @FXML
    private TextArea EventInfoTextArea;

    @FXML
    private Button ButtonChangeRole;

    @FXML
    private Button ButtonSignupAccept;

    @FXML
    private ListView EventList;

    @FXML
    private ListView AdminList;

    @FXML
    private ListView OrganizersList;

    @FXML
    private TextField NameTextField;

    @FXML
    private TextField TimeStartTextField;

    @FXML
    private TextField TimeEndTextField;

    @FXML
    private TextField LocationTextField;

    @FXML
    private TextArea AboutTextArea;

    @FXML
    private TextArea EventViewerTextArea;

    @FXML
    private ColorPicker ColourPickerEvent;

    private int listRole = 0;
    private int roleChange = 0;

    private final int gridVerticalSize = 20;

    private final int gridHorizontalSize = 264;



    public static void start(Person person) {
        if (person.getRole() == 1){
            Main.scheduleScene.getStylesheets().add("resources/visible-tab-pane.css");

        }
        if (person.getRole() == 0){
            Main.scheduleScene.getStylesheets().add("resources/invisible-tab-pane.css");
        }
        Main.stage.setFullScreen(true);
    }



    public void addRows() throws SQLException {
        int size = (24 * 4) + 1;
        int min = 0;
        int hour = 0;
        Text DateText;
        String text;
        RowConstraints row;
        calenderGridPane.getRowConstraints().clear();
        String[] dayName = {" time/date", " Monday", " Tuesday", " Wednesday", " Thursday", " Friday", " Saturday", " Sunday"};



        for ( int i = 1; i <= size; i++ ) {

            text = " " + hour + ":" + min;

            if (min == 0){
                text += "0";
            }
            DateText = new Text(text);

            row = new RowConstraints();

            row.setMaxHeight(gridVerticalSize);
            row.setMinHeight(gridVerticalSize);


            calenderGridPane.setGridLinesVisible(true);
            calenderGridPane.getRowConstraints().add(row);

            calenderGridPane.add(DateText,0,i);

            min += 15;
            if (min == 60 ){
                hour += 1;
                min = 0;
            }
        }

        for ( int i = 0; i <= 7; i++ ){

            text = dayName[i];
            DateText = new Text(text);

            calenderGridPane.setGridLinesVisible(true);
            calenderGridPane.add(DateText,i,0);
        }
    }

    public void addButton(int x, int y) throws SQLException {
        if (y > calenderGridPane.getRowCount()) {
            addRows();
        }
        Button addButton = new Button("this is a button");
        addButton.setOnAction(this::TestEvent);
        calenderGridPane.add(addButton, x, y);
    }

    public void TestEvent(ActionEvent actionEvent){

    }


    public void SignupRefreshEvent(ActionEvent actionEvent) throws SQLException{
        int start = listRole;
        int stop = start;

        if (start == 3){
            start = 0;
            stop = 1;
        }

        SignupList.getItems().clear();
        ArrayList <Person> personArr = SQLite.getFromTable(start, stop);

        for(int i = 0; i < personArr.size(); i++){
            SignupList.getItems().add(personArr.get(i).getEmail());
        }
    }

    public void SignupRefresh() throws SQLException{
        int start = listRole;
        int stop = start;

        if (start == 3){
            start = 0;
            stop = 1;
        }

        SignupList.getItems().clear();
        ArrayList <Person> personArr = SQLite.getFromTable(start, stop);

        for(int i = 0; i < personArr.size(); i++){
            SignupList.getItems().add(personArr.get(i).getEmail());
        }
    }

    public int[] timeStringToInt(String string){
        String []arrayString = string.split(",");
        int []arrayInt = {0,0,0};
        for ( int i = 0; i < arrayString.length; i++ ){
            arrayInt[i] = (Integer.parseInt(arrayString[i]));
        }
        return arrayInt;
    }

    public void setEventInCalendar(String Name, int size) throws SQLException {
        Event event = SQLite.selectFromTableEvents(Name);
        int []arrayBegin = timeStringToInt(event.getTimeBegin());
        int []arrayEND = timeStringToInt(event.getTimeEnd());

        int diff = ((arrayEND[1] - arrayBegin[1]) * 4 )+ ((arrayEND[2] - arrayBegin[2]) / 15) + 1;

        int placement = (arrayBegin[1] * 4) + (arrayBegin[2] / 15);

        System.out.println(event.getColour());
        Rectangle rectangle = new Rectangle(gridHorizontalSize, diff * size, Color.web(event.getColour()));

        rectangle.setOnMouseClicked(e -> {
            System.out.println("test");

            String text = ("Name: " + event.getName() + "\r\nOrganiser: " + event.getOrganisersString() + "\r\nTime: " + event.getTimeBegin() + "-" + event.getTimeEnd() + "\r\nLocation: " + event.getLocation() + "\n\rAbout: " + event.getAbout());
            EventViewerTextArea.setText(text);

            TabPane.getSelectionModel().select(1);
        });

        Text text = new Text(event.getName());

        calenderGridPane.add(rectangle, arrayBegin[0] + 1, placement + 1);

        javafx.scene.layout.GridPane.setMargin(rectangle,new Insets(diff*(size*(2.0/3.0)),0,0,0));
        calenderGridPane.add(text,arrayBegin[0] + 1, (placement+(diff/2)+1));
    }

    public void SignupAcceptEvent(ActionEvent actionEvent) throws SQLException {
        SQLite.changeValueInTable(SignupList.getFocusModel().getFocusedItem().toString(), 0);
        SignupRefresh();
    }

    public void SignupRejectEvent(ActionEvent actionEvent) throws SQLException {
        SQLite.removeFromTable(SignupList.getFocusModel().getFocusedItem().toString());
        SignupRefresh();
    }

    public void ListViewSelectEvent(MouseEvent mouseEvent) throws SQLException {
        String pEmail = SignupList.getFocusModel().getFocusedItem().toString();
        Person person = SQLite.selectFromTablePerson(pEmail);
        String text = ("Name: " + person.getName() + "\r\nAge: " + person.getAge() + "\r\nEmail: " + person.getEmail() + "\n\rAddress: " + person.getAddress() + "\n\rNationality: " + person.getNationality());
        PersonInfoTextArea.setText(text);
        ButtonChangeRole.setText( person.getRole() == 0 ? "set to Admin": "set to Volunteer"  );
        roleChange = person.getRole() == 0 ? 1: 0;
    }

    public void EventViewSelectEvent(MouseEvent mouseEvent) throws SQLException {
        EventInfoTextArea.clear();
        AdminList.getItems().clear();
        OrganizersList.getItems().clear();

        String eventName = EventList.getFocusModel().getFocusedItem().toString();
        System.out.println(eventName);
        Event event = SQLite.selectFromTableEvents(eventName);
        String text = ("Name: " + event.getName() + "\r\nOrganiser: " + event.getOrganisersString() + "\r\nTime: " + event.getTimeBegin() + "-" + event.getTimeEnd() + "\r\nLocation: " + event.getLocation() + "\n\rColour: " + event.getColour() + "\n\rAbout: " + event.getAbout());
        EventInfoTextArea.setText(text);
        ArrayList<Person> personArrayList = SQLite.getFromTable(1, 1);
        ArrayList<String> organisers = event.getOrganisers();

        for ( int i = 0; i < organisers.size(); i++ ) {
            OrganizersList.getItems().add(organisers.get(i));
        }


        for ( int i = 0; i < organisers.size(); i++ ) {
            for ( int j = 0; j < personArrayList.size(); j++ ) {
                if (personArrayList.get(j).getName().equals(organisers.get(i))) {
                    personArrayList.remove(j);
                    j--;
                }
            }
        }
        for ( int i = 0; i < personArrayList.size(); i++){
            AdminList.getItems().add(personArrayList.get(i).getName());
        }
    }


    public void changeUserRoleEvent(ActionEvent actionEvent) throws SQLException {
        SQLite.changeValueInTable(SignupList.getFocusModel().getFocusedItem().toString(),roleChange);
        SignupRefresh();
    }

    public void SeeAdminsEvent(ActionEvent actionEvent) throws SQLException {
        ButtonChangeRole.setVisible(true);
        ButtonSignupAccept.setVisible(false);
        listRole = 1;
        SignupRefresh();
    }

    public void SeeActiveUsersEvent(ActionEvent actionEvent) throws SQLException {
        ButtonChangeRole.setVisible(true);
        ButtonSignupAccept.setVisible(false);
        listRole = 3;
        SignupRefresh();
    }

    public void SeeVolunteersEvent(ActionEvent actionEvent) throws SQLException {
        ButtonChangeRole.setVisible(true);
        ButtonSignupAccept.setVisible(false);
        listRole = 0;
        SignupRefresh();
    }

    public void ShowStandbyUsersEvent(ActionEvent actionEvent) throws SQLException {
        ButtonChangeRole.setVisible(false);
        ButtonSignupAccept.setVisible(true);
        listRole = 2;
        SignupRefresh();
    }

    public void EventListRefreshEvent(ActionEvent actionEvent) throws SQLException {
        EventList.getItems().clear();
        AdminList.getItems().clear();

        ArrayList<Event> eventArrayList = SQLite.getFromTableEvents();
        ArrayList<Person> personArrayList = SQLite.getFromTable(1,1);

        for ( int i = 0; i < eventArrayList.size(); i++ ){
            EventList.getItems().add(eventArrayList.get(i).getName());
        }

        for ( int i = 0; i < personArrayList.size(); i++ ){
            AdminList.getItems().add(personArrayList.get(i).getName());
        }
    }

    public void EventListRefresh() throws SQLException{
        EventList.getItems().clear();

        ArrayList<Event> arrayList = SQLite.getFromTableEvents();

        for ( int i = 0; i < arrayList.size(); i++ ){
            EventList.getItems().add(arrayList.get(i).getName());
        }
    }

    public void DeleteEventEvent(ActionEvent actionEvent) throws SQLException {
        SQLite.removeFromTableEvent(EventList.getFocusModel().getFocusedItem().toString());

        EventListRefresh();
    }

    public void AddAsOrganiserEvent(ActionEvent actionEvent) throws SQLException {
        Event event = SQLite.selectFromTableEvents(EventList.getFocusModel().getFocusedItem().toString());

        String organisers = event.getOrganisersString();

        organisers += AdminList.getFocusModel().getFocusedItem().toString() + ",";

        event.setOrganisersToArray(organisers);

        SQLite.changeValueInTableEvents(event.getName(), event.getOrganisersString());

        refreshOrganisers();

    }

    public void RemoveAsOrganiserEvent(ActionEvent actionEvent) throws SQLException {
        Event event = SQLite.selectFromTableEvents(EventList.getFocusModel().getFocusedItem().toString());

        String remove = OrganizersList.getFocusModel().getFocusedItem().toString();

        ArrayList arrayList = event.getOrganisers();

        for ( int i = 0; i < arrayList.size(); i++ ){
            if (arrayList.get(i).equals(remove)){
                arrayList.remove(i);
                break;
            }
        }
        event.setOrganisers(arrayList);
        SQLite.changeValueInTableEvents(event.getName(),event.getOrganisersString());
        refreshOrganisers();
    }

    public void refreshOrganisers() throws SQLException {
        EventInfoTextArea.clear();
        AdminList.getItems().clear();
        OrganizersList.getItems().clear();

        String eventName = EventList.getFocusModel().getFocusedItem().toString();
        System.out.println(eventName);
        Event event = SQLite.selectFromTableEvents(eventName);
        String text = ("Name: " + event.getName() + "\r\nOrganiser: " + event.getOrganisersString() + "\r\nTime: " + event.getTimeBegin() + "-" + event.getTimeEnd() + "\r\nLocation: " + event.getLocation() + "\n\rColour: " + event.getColour() + "\n\rAbout: " + event.getAbout());
        EventInfoTextArea.setText(text);
        ArrayList<Person> personArrayList = SQLite.getFromTable(1, 1);
        ArrayList<String> organisers = event.getOrganisers();

        for ( int i = 0; i < organisers.size(); i++ ) {
            OrganizersList.getItems().add(organisers.get(i));
        }


        for ( int i = 0; i < organisers.size(); i++ ) {
            for ( int j = 0; j < personArrayList.size(); j++ ) {
                if (personArrayList.get(j).getName().equals(organisers.get(i))) {
                    personArrayList.remove(j);
                    j--;
                }
            }
        }
        for ( int i = 0; i < personArrayList.size(); i++){
            AdminList.getItems().add(personArrayList.get(i).getName());
        }
    }

    public void MakeEventEvent(ActionEvent actionEvent) throws SQLException {
        if( !NameTextField.getText().isEmpty() || !AboutTextArea.getText().isEmpty() || !TimeEndTextField.getText().isEmpty() || !TimeStartTextField.getText().isEmpty() || !LocationTextField.getText().isEmpty()){
            Event event = new Event();
            event.setTimeBegin(TimeStartTextField.getText());
            event.setTimeEnd(TimeEndTextField.getText());
            event.setLocation(LocationTextField.getText());
            event.setName(NameTextField.getText());
            event.setAbout(AboutTextArea.getText());
            SQLite.insertToTable(event);
            NameTextField.clear();
            TimeStartTextField.clear();
            TimeEndTextField.clear();
            LocationTextField.clear();
            AboutTextArea.clear();
            return;
        }

        NameTextField.setPromptText("fill all the fields");
        TimeStartTextField.setPromptText("fill all the fields");
        TimeEndTextField.setPromptText("fill all the fields");
        LocationTextField.setPromptText("fill all the fields");
        AboutTextArea.setPromptText("fill all the fields");
    }

    public void ClearTextFieldsEvent(ActionEvent actionEvent){
        NameTextField.clear();
        TimeStartTextField.clear();
        TimeEndTextField.clear();
        LocationTextField.clear();
        AboutTextArea.clear();
    }

    public void BackToScheduleEvent(ActionEvent actionEvent){
        TabPane.getSelectionModel().select(0);
    }

    public void SetColourEvent(ActionEvent actionEvent) throws SQLException {

        addRows();
        System.out.println(ColourPickerEvent.getValue());
        SQLite.changeColourInTableEvents(EventList.getFocusModel().getFocusedItem().toString() ,ColourPickerEvent.getValue().toString());
        ArrayList<Event> arrayList = SQLite.getFromTableEvents();

        for ( int i = 0; i < arrayList.size(); i++){
            setEventInCalendar(arrayList.get(i).getName(), gridVerticalSize);
        }
        EventListRefresh();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            addRows();
            ArrayList<Event> arrayList = SQLite.getFromTableEvents();

            for ( int i = 0; i < arrayList.size(); i++){
                setEventInCalendar(arrayList.get(i).getName(), gridVerticalSize);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}