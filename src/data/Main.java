package data;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;

public class Main extends Application {

    public static Stage stage;
    public static Scene loginScene;
    public static Scene scheduleScene;
    public static ScheduleController scheduleController;
    public static LoginController loginController;

    public static void main(String[] args) throws SQLException {
        SQLite.connect();
        SQLite.createTablePeople();
        SQLite.createTableEvents();
        if (SQLite.isTableEmpty(1)){
            Admin admin = new Admin();
            admin.setName("ADMIN");
            admin.setAge(99);
            admin.setAddress("ADMIN");
            admin.setNationality("ADMIN");
            admin.setEmail("ADMIN@ADMIN.com");
            admin.setPassword("Admin123");
            SQLite.insertToTable(admin);
        }
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {

        Main.stage = stage;

        FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("../FXML/LoginPage.fxml"));
        Parent loginView = loginLoader.load();
        loginController = loginLoader.getController();

        FXMLLoader scheduleLoader = new FXMLLoader(getClass().getResource("../FXML/SchedulePage.fxml"));
        Parent scheduleView = scheduleLoader.load();
        scheduleController = scheduleLoader.getController();

        loginScene = new Scene(loginView, 600, 450);
        scheduleScene = new Scene(scheduleView, 600, 450);

        loginScene.getStylesheets().add("resources/invisible-tab-pane.css");
        scheduleScene.getStylesheets().add("resources/invisible-tab-pane.css");
        stage.setTitle("Roskilde");
        stage.setScene(loginScene);
        stage.setMinHeight(450);
        stage.setMinWidth(600);
        stage.show();
    }



}