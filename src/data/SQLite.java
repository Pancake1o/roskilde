package data;
import java.sql.*;
import java.util.ArrayList;

public class SQLite {

    private static String dataBaseName = "jdbc:sqlite:PersonsFile.db";

    public static void connect() throws SQLException {
        Connection c = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
        } catch (Exception e){
            System.err.println(e.getClass().getName()+": " + e.getMessage());
            System.exit(0);
        } finally {
            c.close();
        }
        System.out.println("connected");
    }

    public static void createTablePeople() throws SQLException {
        // create table if it does not exist
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);

            stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS ROSKILDE" +
                    "(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " NAME           TEXT   NOT NULL, " +
                    " AGE            INT    NOT NULL, " +
                    " ADDRESS        TEXT   NOT NULL, " +
                    " NATIONALITY    TEXT   NOT NULL, " +
                    " EMAIL          TEXT   NOT NULL UNIQUE, " +
                    " PASSWORD       TEXT   NOT NULL, " +
                    " ROLE           INT    NOT NULL)";
            stmt.executeUpdate(sql);



        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static void insertToTable(Person person) throws SQLException {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "INSERT INTO ROSKILDE (NAME,AGE,ADDRESS,NATIONALITY,EMAIL,PASSWORD,ROLE) " +
                    "VALUES ('"/* + person.getId() + ", '"*/ + person.getName() + "', " + person.getAge() + ", '" + person.getAddress() + "', '" + person.getNationality() + "', '" + person.getEmail() + "', '" + person.getPassword() + "', " + person.getRole() + ");";
            stmt.executeUpdate(sql);
        } catch ( Exception e ){
            System.err.println( e.getClass().getName() +": " + e.getMessage());
            //System.exit(0);
        } finally {
            stmt.close();
            c.commit();
            c.close();
        }
    }


    public static void printFromTable(int id) throws SQLException {

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE" + " WHERE ID = " + id);

            System.out.println("ID: " + rs.getInt("ID"));
            System.out.println("NAME,: " + rs.getString("NAME"));
            System.out.println("AGE: " + rs.getInt("AGE"));
            System.out.println("ADDRESS: " + rs.getString("ADDRESS"));
            System.out.println("NATIONALITY: " + rs.getString("NATIONALITY"));
            System.out.println("EMAIL: " + rs.getString("EMAIL"));
            System.out.println("PASSWORD: " + rs.getString("PASSWORD"));
            System.out.println("ROLE: " + rs.getInt("ROLE"));

        } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
        } finally {
            stmt.close();
            c.close();
        }

    }
    public static Person selectFromTablePerson(int id) throws SQLException {

        Person person = new Person();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE" + " WHERE ID = " + id);

            person.setId(rs.getInt("ID"));
            person.setName(rs.getString("NAME"));
            person.setAge(rs.getInt("AGE"));
            person.setAddress(rs.getString("ADDRESS"));
            person.setNationality(rs.getString("NATIONALITY"));
            person.setEmail(rs.getString("EMAIL"));
            person.setPassword(rs.getString("PASSWORD"));
            person.setRole(rs.getInt("ROLE"));


            return person;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
            //System.exit(0);
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static Person selectFromTablePerson(String email) throws SQLException {

        Person person = new Person();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE WHERE EMAIL = '" + email +"'");

            person.setId(rs.getInt("ID"));
            person.setName(rs.getString("NAME"));
            person.setAge(rs.getInt("AGE"));
            person.setAddress(rs.getString("ADDRESS"));
            person.setNationality(rs.getString("NATIONALITY"));
            person.setEmail(rs.getString("EMAIL"));
            person.setPassword(rs.getString("PASSWORD"));
            person.setRole(rs.getInt("ROLE"));

            return person;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return person;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static Boolean isTableEmpty(int id) throws SQLException {

        Person person = new Person();
        Boolean isEmpty;
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE;");


            person.setId(rs.getInt("ID"));

            if (person.getId() >= 0){
                isEmpty = false;

            } else {
                isEmpty = true;
            }

            return isEmpty;
        } catch (Exception e) {

            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            isEmpty = true;
            return isEmpty;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static Boolean isEmailInTable(String email) throws SQLException {


        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE WHERE EMAIL = '" + email +"'");

            if (email.equals(rs.getString("EMAIL"))){
                return true;
            }

            return false;



        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return false;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static ArrayList getFromTable(int role1, int role2) throws SQLException {

        Person person;
        ArrayList personArr = new ArrayList();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ROSKILDE WHERE ROLE >= '" + role1 +"' AND Role <= '" + role2 + "' AND ID > 1");

            while (rs.next()){

                person = new Person();
                person.setId(rs.getInt("ID"));
                person.setName(rs.getString("NAME"));
                person.setAge(rs.getInt("AGE"));
                person.setAddress(  rs.getString("ADDRESS"));
                person.setNationality(rs.getString("NATIONALITY"));
                person.setEmail(rs.getString("EMAIL"));
                person.setPassword(rs.getString("PASSWORD"));
                person.setRole(rs.getInt("ROLE"));
                personArr.add(person);
            }


            return personArr;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return personArr;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static void changeValueInTable(String email, int role) throws SQLException {

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "UPDATE ROSKILDE SET ROLE = " + role + " WHERE EMAIL = '" + email +"'";
           stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }


    public static void removeFromTable(String email) throws SQLException {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql ="DELETE FROM ROSKILDE WHERE EMAIL = '" + email +"'";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }

    public static void createTableEvents() throws SQLException {
        // create table if it does not exist
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS EVENTS" +
                    "(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " NAME           TEXT   NOT NULL, " +
                    " START          TEXT   NOT NULL, " +
                    " STOP           TEXT   NOT NULL, " +
                    " LOCATION       TEXT   NOT NULL, " +
                    " ABOUT          TEXT   NOT NULL, " +
                    " ORGANIZERS     TEXT   NOT NULL, " +
                    " COLOUR         TEXT   NOT NULL)";
            stmt.executeUpdate(sql);



        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }

    public static void insertToTable(Event event) throws SQLException {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "INSERT INTO EVENTS (NAME,START,STOP,LOCATION,ABOUT,ORGANIZERS,COLOUR) " +
                    "VALUES ('"/* + person.getId() + ", '"*/ + event.getName() + "', '" + event.getTimeBegin() + "', '" + event.getTimeEnd() + "', '" + event.getLocation() + "', '" + event.getAbout() + "', '" + event.getOrganisersString() + "','#000000');";
            stmt.executeUpdate(sql);
        } catch ( Exception e ){
            System.err.println( e.getClass().getName() +": " + e.getMessage());
            //System.exit(0);
        } finally {
            stmt.close();
            c.commit();
            c.close();
        }
    }

    public static Event selectFromTableEvents(String name) throws SQLException {

        Event event = new Event();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS WHERE NAME = '" + name +"'");

            event.setId(rs.getInt("ID"));
            event.setName(rs.getString("NAME"));
            event.setTimeBegin(rs.getString("START"));
            event.setTimeEnd(rs.getString("STOP"));
            event.setAbout(rs.getString("ABOUT"));
            event.setLocation(rs.getString("LOCATION"));
            event.setOrganisersToArray(rs.getString("ORGANIZERS"));
            event.setColour(rs.getString("COLOUR"));

            return event;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return event;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static Event selectFromTableEvents(int id) throws SQLException {

        Event event = new Event();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS WHERE ID = " + id);

            event.setId(rs.getInt("ID"));
            event.setName(rs.getString("NAME"));
            event.setTimeBegin(rs.getString("START"));
            event.setTimeEnd(rs.getString("STOP"));
            event.setAbout(rs.getString("LOCATION"));
            event.setLocation(rs.getString("ABOUT"));
            event.setOrganisersToArray(rs.getString("ORGANIZERS"));
            event.setColour(rs.getString("COLOUR"));

            return event;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return event;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static ArrayList<Event> getFromTableEvents() throws SQLException {


        Event event;
        ArrayList arrayList = new ArrayList<Event>();
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS");

            while (rs.next()){
                event = new Event();

                event.setId(rs.getInt("ID"));
                event.setName(rs.getString("NAME"));
                event.setTimeBegin(rs.getString("START"));
                event.setTimeEnd(rs.getString("STOP"));
                event.setAbout(rs.getString("LOCATION"));
                event.setLocation(rs.getString("ABOUT"));
                event.setOrganisersToArray(rs.getString("ORGANIZERS"));
                event.setColour(rs.getString("COLOUR"));

                arrayList.add(event);
            }

            return arrayList;

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
            return arrayList;
        } finally {
            stmt.close();
            c.close();
        }
    }

    public static void removeFromTableEvent(String name) throws SQLException {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql ="DELETE FROM EVENTS WHERE NAME = '" + name +"'";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }

    public static void changeValueInTableEvents(String name, String organizers) throws SQLException {

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "UPDATE EVENTS SET ORGANIZERS = '" + organizers + "' WHERE NAME = '" + name +"'";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }
    public static void changeColourInTableEvents(String name, String colour) throws SQLException {

        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dataBaseName);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = "UPDATE EVENTS SET COLOUR = '" + colour + "' WHERE NAME = '" + name +"'";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        } finally {
            c.commit();
            stmt.close();
            c.close();
        }
    }
}
