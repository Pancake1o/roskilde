package data;

import java.util.ArrayList;

public class Event {
    //Event(){
        //setColour("#000000");
   //}

    int id;
    private String timeBegin;
    private String timeEnd;
    private String name;
    private String about;
    private ArrayList organisers = new ArrayList();
    private String location;
    private String colour;

    public String getOrganisersString(){
        String org = "";
        for ( int i = 0; i < organisers.size(); i++ ){
            org += organisers.get(i) + ",";
        }
        return org;
    }

    public void setOrganisersToArray(String commaString ){

        ArrayList arrayList = new ArrayList<>();

        String []array = commaString.split(",");

        for ( int i = 0; i < array.length; i++ ){
            arrayList.add(array[i]);
        }
        setOrganisers(arrayList);
    }


    public String getTimeBegin() {
        return timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public String getAbout() {
        return about;
    }

    public String getName() {
        return name;
    }

    public ArrayList getOrganisers() {
        return organisers ;
    }

    public String getLocation(){return  location;}

    public void setAbout(String about) {
        this.about = about;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void setOrganisers(ArrayList organisers) {
        this.organisers = organisers;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
