package data;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;

import java.sql.SQLException;


 public class LoginController {

    @FXML
    private TabPane TabPaneLogin;

    @FXML
    private TextField InputLoginEmail;

    @FXML
    private TextField InputLoginPassword;

    @FXML
    private TextField InputSignupName;

    @FXML
    private TextField InputSignupAge;

    @FXML
    private TextField InputSignupAddress;

    @FXML
    private TextField InputSignupNationality;

    @FXML
    private TextField InputSignupEmail;

    @FXML
    private TextField InputSignupPassword;

    @FXML
    private TextField InputSignupRetypePassword;



     public void loginEvent(ActionEvent actionEvent) throws SQLException {
         String e = InputLoginEmail.getText();
         String p =  InputLoginPassword.getText();

         login(e, p);
    }

    public void login(String email, String password) throws SQLException {
         Person person;
         person = SQLite.selectFromTablePerson(email);
         if (person.getPassword().equals(password) ){
            if (person.getRole() < 2){
                Main.stage.setScene(Main.scheduleScene);
                System.out.println("hello");
                ScheduleController.start(person);
            } else {
                InputLoginEmail.setPromptText("your user has not ben validated yet");
                InputLoginEmail.clear();
                InputLoginPassword.setPromptText("your user has not ben validated yet");
                InputLoginPassword.clear();
            }
        } else {
            InputLoginEmail.setPromptText("Wrong Email Or Password");
            InputLoginEmail.clear();
            InputLoginPassword.setPromptText("Wrong Email Or Password");
            InputLoginPassword.clear();
        }
    }

    public void SignupEventBegin(ActionEvent actionEvent){
        TabPaneLogin.getSelectionModel().select(1);
    }

    public void SignupEventFinish(ActionEvent actionEvent) throws SQLException {
        String e = InputSignupEmail.getText();
        String p =  InputSignupPassword.getText();
        String rp = InputSignupRetypePassword.getText();
        String n = InputSignupName.getText();
        String N = InputSignupNationality.getText();
        int a = Integer.parseInt(InputSignupAge.getText());
        String A = InputSignupAddress.getText();

        if (p.equals(rp) && p.length() > 4){
            if (e.length() > 4 && !SQLite.isEmailInTable(e)){
                if (n.length() > 4 && N.length() > 4 && A.length() > 4 && a < 100 && a > 16){
                    Person signup = new Person();

                    signup.setPassword(p);
                    signup.setRole(2);
                    signup.setNationality(N);
                    signup.setAge(a);
                    signup.setAddress(A);
                    signup.setName(n);
                    signup.setEmail(e);

                    SQLite.insertToTable(signup);
                    TabPaneLogin.getSelectionModel().select(0);
                }
            }
        }
    }
    public void SignupEventReturn(ActionEvent actionEvent){
        TabPaneLogin.getSelectionModel().select(0);
    }
}
